# Recipie APP API Proxy

NGINX proxy app for our recipie app API

## Usage

### Environment Variables

- `LISTEN_PORT` - The port to listen on. Defaults to `8080`
- `APP_HOST` - The host of the API to proxy to. Defaults to `app`
- `APP_PORT` - The port of the API to proxy to. Defaults to `9000`
